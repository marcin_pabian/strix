<?php


namespace Application\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\TripMeasureRepository")
 * @ORM\Table(name="trip_measures")
 */
class TripMeasure
{
    use EntityIdTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Trip", inversedBy="measures")
     * @ORM\JoinColumn(name="trip_id", referencedColumnName="id")
     */
    protected $trip;

    /**
     * @ORM\Column(name="distance", precision=5, scale=2, nullable=false)
     */
    protected $distance;

    public function getDistance(): float
    {
        return $this->distance;
    }
}