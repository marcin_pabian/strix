## Installation

Copy `config/autoload/local.php.dist` to `config/autoload/local.php` and change database connection parameters.

Run:

```bash
$ composer deploy
```

Visit [localhost:8080](http://localhost:8080/) in your web browser