<?php


namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\TripRepository")
 * @ORM\Table(name="trips")
 */
class Trip
{
    use EntityIdTrait;

    /**
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="measure_interval", type="integer", )
     */
    protected $measureInterval;

    /**
     * @ORM\OneToMany(targetEntity="TripMeasure", mappedBy="trip")
     */
    protected $measures;

    public function __construct()
    {
        $this->measures = new ArrayCollection;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMeasureInterval(): int
    {
        return $this->measureInterval;
    }

    /**
     * @return TripMeasure[]
     */
    public function getMeasuresArrayCopy(): array
    {
        return $this->measures->toArray();
    }
}