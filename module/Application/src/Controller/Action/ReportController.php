<?php


namespace Application\Controller\Action;


use Application\Service\ReportGenerator;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ReportController extends AbstractActionController
{
    protected $reportGenerator;

    public function __construct(ReportGenerator $reportGenerator)
    {
        $this->reportGenerator = $reportGenerator;
    }

    public function reportAction(): ViewModel
    {
        return new ViewModel([
            'report' => $this->reportGenerator->getReport()
        ]);
    }
}