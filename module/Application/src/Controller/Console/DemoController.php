<?php


namespace Application\Controller\Console;


use Application\Entity\Trip;
use Application\Entity\TripMeasure;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Zend\Console\ColorInterface;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class DemoController extends AbstractConsoleController
{
    protected $tripRepository, $tripMeasureRepository;

    public function __construct(EntityManager $entityManager)
    {
        $this->tripRepository = $entityManager->getRepository(Trip::class);
        $this->tripMeasureRepository = $entityManager->getRepository(TripMeasure::class);
    }

    public function initAction(): void
    {
        try {
            $this->loadDemoTrips();
            $this->loadDemoTripMeasures();
        } catch (UniqueConstraintViolationException $exception) {
            $this->console->writeLine('Load demo fail! Are database filled already?', ColorInterface::RED);
            exit(1);
        }

        $this->console->writeLine('Load demo done!', ColorInterface::GREEN);
    }

    private function loadDemoTrips(): void
    {
        $this->tripRepository->loadDemoData();
    }

    private function loadDemoTripMeasures(): void
    {
        $this->tripMeasureRepository->loadDemoData();
    }
}