<?php


namespace Application\Service;


use Application\DTO\Trip as DTO;
use Application\Entity\Repository\TripRepository;
use Application\Entity\Trip;
use Application\Entity\TripMeasure;

class ReportGenerator
{
    protected $tripRepository;

    public function __construct(TripRepository $tripRepository)
    {
        $this->tripRepository = $tripRepository;
    }

    /**
     * @return DTO[]
     */
    public function getReport(): array
    {
        $data = [];
        $trips = $this->tripRepository->findAll();

        if (empty($trips) === true) return $data;

        /** @var Trip $trip */
        foreach ($trips as $trip) {
            $distance = $this->calculateDistance($trip->getMeasuresArrayCopy());
            $avgSpeed = $this->calculateAvgSpeed($trip->getMeasuresArrayCopy(), $trip->getMeasureInterval());
            $data[] = new DTO($trip->getName(), $distance, $trip->getMeasureInterval(), $avgSpeed);
        }

        return $data;
    }

    private function calculateDistance(array $measures): float
    {
        if (count($measures) <= 1) return 0;

        return max(
            array_map(
                function (TripMeasure $measure) {
                    return $measure->getDistance();
                },
                $measures
            )
        );
    }

    private function calculateAvgSpeed(array $measures, int $measureInterval): int
    {
        $avgSpeed = 0;

        if (count($measures) <= 1) return $avgSpeed;

        usort($measures, function (TripMeasure $a, TripMeasure $b) {
            return $a->getDistance() <=> $b->getDistance();
        });

        $measures = array_values($measures);

        for ($i = 0; $i < max(array_keys($measures)); $i++) {
            $speed = ($measures[$i + 1]->getDistance() - $measures[$i]->getDistance()) * 3600 / $measureInterval;
            $avgSpeed = $avgSpeed >= $speed ? $avgSpeed : $speed;
        }

        return floor($avgSpeed);
    }


}