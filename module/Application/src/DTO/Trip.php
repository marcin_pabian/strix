<?php


namespace Application\DTO;


class Trip
{
    protected $name, $distance, $measureInterval, $avgSpeed;

    public function __construct(string $name, float $distance, int $measureInterval, int $avgSpeed)
    {
        $this->name = $name;
        $this->distance = $distance;
        $this->measureInterval = $measureInterval;
        $this->avgSpeed = $avgSpeed;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDistance(): float
    {
        return $this->distance;
    }

    public function getMeasureInterval(): int
    {
        return $this->measureInterval;
    }

    public function getAvgSpeed(): float
    {
        return $this->avgSpeed;
    }
}