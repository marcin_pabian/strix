<?php


namespace Application\Service\Factory;


use Application\Entity\Trip;
use Application\Service\ReportGenerator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ReportGeneratorFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ReportGenerator(
            $container->get(EntityManager::class)->getRepository(Trip::class)
        );
    }
}