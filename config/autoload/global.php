<?php

use Doctrine\DBAL\Driver\PDOMySql\Driver;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => Driver::class,
                'params' => [
                    'driverOptions' => [
                        '1002' => 'SET NAMES utf8'
                    ]
                ]
            ]
        ]
    ]
];
