<?php


namespace Application\Entity\Repository;


use Doctrine\ORM\EntityRepository;

class TripRepository extends EntityRepository
{
    public function loadDemoData(): void
    {
        $query =  <<<QUERY

INSERT INTO `trips` (`id`, `name`, `measure_interval`) VALUES
(1, 'Trip 1', 15),
(2, 'Trip 2', 20),
(3, 'Trip 3', 12);

QUERY;

        $this->getEntityManager()->getConnection()->executeUpdate($query);
    }
}